import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-view-encaps-parents',
  templateUrl: './view-encaps-parents.component.html',
  styleUrls: ['./view-encaps-parents.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class ViewEncapsParentsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
