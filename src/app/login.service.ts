import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor() { }
  currentUser:any;

  logIn(username:string,password:string){
    localStorage.setItem('userName',username);
    if(username==='Admin'){
      this.currentUser={
        id:'admin@gmail.com',
        uname:'admin',
        isAdmin:true
      };
      return;

    }
    this.currentUser={
      id:username+'@gmail.com',
      uname:username,
      isAdmin:false
    };

  }
}
