import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

@Component({
  selector: 'app-departmrnt-details',
  templateUrl: './departmrnt-details.component.html',
  styleUrls: ['./departmrnt-details.component.scss']
})
export class DepartmrntDetailsComponent implements OnInit {
  selectedDepartment:any;

  constructor( private activeRoute:ActivatedRoute,private router:Router) { }

  ngOnInit(): void {
    //this.selectedDepartment=this.activeRoute.snapshot.paramMap.get('depId');
    this.activeRoute.paramMap.subscribe((param:ParamMap)=> {
      this.selectedDepartment=param.get('depId');
    });

    
  }
  onPrevious(){
    let previousId=(+this.selectedDepartment) -1;
    this.router.navigate(['/depdetails',previousId]);

  }

  onNext(){
    let nextId=(+this.selectedDepartment) +1;
    this.router.navigate(['/depdetails',nextId]);

  }
  onBack(){
    let id=this.selectedDepartment ? this.selectedDepartment:null;
    //Passing a optional paramete 'id'. so we can read it in department componet anf make use it to
    //highlight selected department
    this.router.navigate(['/departments',{'id':id}]);
  }

  onOverview(){
    this.router.navigate(['overview'],{relativeTo:this.activeRoute});

  }

  onContact(){
    this.router.navigate(['contact'],{relativeTo:this.activeRoute});

  }

}
