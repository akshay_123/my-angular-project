import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-employeetemplateform',
  templateUrl: './employeetemplateform.component.html',
  styleUrls: ['./employeetemplateform.component.scss']
})
export class EmployeetemplateformComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onSubmitForm(myForm:NgForm){
    console.log(myForm);
    myForm.reset();
    
  }

}
