import { LogoutService } from './../logout.service';
import { LoginService } from './../login.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private loginService:LoginService,private logoutService:LogoutService) { }

  ngOnInit(): void {
  }

  login(){
    this.loginService.logIn('Admin','1234');
    console.log('loggedin');

  }

  logout(){
    this.logoutService.logOut();
    console.log('loggedout')

  }

}
