import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-cpc-child',
  templateUrl: './cpc-child.component.html',
  styleUrls: ['./cpc-child.component.scss']
})
export class CpcChildComponent implements OnInit {

  @Input()selectedProductFromParent:string='';
  @Input()flag:boolean=false;
  @Output()myEvent= new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  onClick(val:string){
    this.flag=false;
    this.myEvent.emit(val);

  }

}
