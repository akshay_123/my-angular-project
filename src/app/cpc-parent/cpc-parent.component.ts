import { Component, OnInit } from '@angular/core';
import { Products } from './products';

@Component({
  selector: 'app-cpc-parent',
  templateUrl: './cpc-parent.component.html',
  styleUrls: ['./cpc-parent.component.scss']
})
export class CpcParentComponent implements OnInit {

  constructor() { }
  flag:boolean=false;

  ngOnInit(): void {
  }

  selectedProduct:string='';
  productArray:Products[]=[
    new Products('1','Shirt'),
    new Products('2','Watch'),
    new Products('3','Shoes')
  ];

  onSelect(item:Products){
    this.flag=true;
    this.selectedProduct=item.product_name;

  }

  eventEmittedFromChild(event:any){
    console.log(event,'from child');
    this.productArray=this.productArray.filter((x)=>x.product_name.match(event) || x.product_id.match(event));

  }

}
