import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {
  num:number=0;
  bool:boolean=false;
  num1:number=0;
  num2:number=0;
  sum:number=0;


  constructor() { }

  ngOnInit(): void {
  }

  onButtonClick(){
    alert("Welcome to LTI");
  }

  onAdditionClick(){
    this.sum=this.num1 + this.num2;

  }

  OnKeyUp(){
    alert("you have typing")
  }

  onKeyDown(){
    alert("you are removing")
  }
  addUsingTemplateBinding(input1:string,input2:string){
    this.sum=(+input1)+(+input2);
  }


}
