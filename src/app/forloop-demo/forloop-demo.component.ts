import { Items } from './items';
import { Grouphotels } from './grouphotels';
import { Hotels } from './hotels';
import { Component, OnInit } from '@angular/core';
import { Employee } from './Employee';

@Component({
  selector: 'app-forloop-demo',
  templateUrl: './forloop-demo.component.html',
  styleUrls: ['./forloop-demo.component.scss']
})
export class ForloopDemoComponent implements OnInit {

  constructor() { }
  empArray:Employee[]=[
    new Employee(1,'Akshay','Dev'),
    new Employee(2,'Ravi','QA'),
    new Employee(3,'Raj','Devops')
  ];

  //Nested for loop demo
  hotelGr1:Hotels[]=[
    new Hotels('A1'),
    new Hotels('A2'),
    new Hotels('A3')
  ];

  hotelGr2:Hotels[]=[
    new Hotels('B1'),
    new Hotels('B2'),
    new Hotels('B3')
  ];

  groupArray:Grouphotels[]=[
    new Grouphotels("Group1",this.hotelGr1),
    new Grouphotels("Group2",this.hotelGr2)
  ];

  //switch case demo
  selctedValue:string='Select';
  items:Items[]=[
    
    new Items('One',1),
    new Items('Select',0),
    new Items('Two',2),
    new Items('Three',3)
  ];
  ngOnInit(): void {
  }

}
