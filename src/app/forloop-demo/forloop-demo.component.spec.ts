import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForloopDemoComponent } from './forloop-demo.component';

describe('ForloopDemoComponent', () => {
  let component: ForloopDemoComponent;
  let fixture: ComponentFixture<ForloopDemoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForloopDemoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForloopDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
