import { NgxSpinnerService } from 'ngx-spinner';
import { NgToastService } from 'ng-angular-popup';
import { Router, ActivatedRoute } from '@angular/router';
import { TodotaskService } from './todotask.service';
import { Status } from './status';
import { Component, OnInit } from '@angular/core';
import { Todo } from './todo';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  taskName: string='';
  taskStatus: string='';

  id:number=0;
  task:String='';
  stsatus:string='';

  selectedStatus:string='Select'
  statuses:Status[]=[
    new Status('Pending'),
    new Status('Done'),
    new Status('Select')
  ];
  arrtodo:Todo[]=[];

  constructor(private todoTaskService:TodotaskService,private router:Router,private toast:NgToastService,private activateRoute:ActivatedRoute,private spinner:NgxSpinnerService) { }

  ngOnInit(): void {
    
    // this.getAllTasks();
    this.arrtodo=this.activateRoute.snapshot.data['todos'];
    
  }
   


  //call to get all task api
    getAllTasks(){
      this.todoTaskService.getAllTasks().subscribe(
        async (data:any) => {
          //await new Promise(f => setTimeout(f, 1000));
          this.arrtodo=data;
        }
      );

    }

    //Call to Add task API
    onAddToDoTask(){
      if(this.selectedStatus=='Select'){
      alert("Please select valid value from dropdown");
      } else{
      let item=this.arrtodo[this.arrtodo.length-1];
      let id=item.id+1
      //Call to add user service
      this.todoTaskService.addTask(new Todo(id,this.taskName,this.selectedStatus)).subscribe((data) => {
        alert('Task added succesfully');
        this.toast.success({detail:'Success Message',summary:'Task added Successfully',duration:5000});
        //window.location.reload();
        this.getAllTasks();
      });
     // this.arrtodo.push(new Todo(id,this.taskName,this.selectedStatus));
      this.taskName='';
      this.taskStatus='';
      }
    }

    //call to delete API
    onDeleteToDoTask(item:Todo){
      alert("confirm delete");
      this.todoTaskService.deleteTask(item.id).subscribe((data) =>{
        this.toast.success({detail:'Success Message',summary:'Task Deleted Successfully',duration:5000});
        this.getAllTasks();

      });
      //this.arrtodo.splice(this.arrtodo.indexOf(item),1);
    }

    //To show existing task to UI
    onEdit(item:Todo){
      this.router.navigate(['todo/edit',item.id]);
      //commented due to created edit seperate component
      // this.stsatus=item.status;
      // this.id=item.id;
      // this.task=item.title;
    }

    //call to update task API
    onEditToDoTask(){ 
      // this.todoTaskService.updateTask(new Todo(this.id,this.task,this.stsatus)).subscribe((data) =>{
      //   this.toast.success({detail:'Success Message',summary:'Task updated Successfully',duration:5000});
      //   this.getAllTasks();
      // });
    }   
}