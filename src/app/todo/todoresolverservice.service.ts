import { TodotaskService } from './todotask.service';
import { Todo } from './todo';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodoresolverserviceService implements Resolve<Todo[]> {

  constructor( private todoService:TodotaskService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):Observable<Todo[]> {
    return this.todoService.getAllTasks().pipe(map((x:any)=>{
      console.log('from todo resolver',x);
      return x;
    }));
  }
}
