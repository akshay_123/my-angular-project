import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Todo } from './todo';

@Injectable({
  providedIn: 'root'
})
export class TodotaskService {
   baseUrl='http://localhost:3000/task/';

  constructor(private _http:HttpClient) { }

  //Get all task service
  getAllTasks():Observable<Object>{
    return this._http.get(this.baseUrl);
  }

  //Add task service
  addTask(task:Todo){
    let requestBody=JSON.stringify(task);
   // let header=new HttpHeaders().set('Content-Type','application/json');
    return this._http.post(this.baseUrl,requestBody);

  }

  //update task service
  updateTask(task:any){
    let requestBody=JSON.stringify(task);
  //  let header=new HttpHeaders().set('Content-Type','application/json');
  //  return this._http.put(this.baseUrl+task.id,requestBody,{headers:header});
  //setting header using http interceptor
    return this._http.put(this.baseUrl+task.id,requestBody);

  }
  deleteTask(id:number){
    return this._http.delete(this.baseUrl+id);
  }

  getTaskById(id:any):Observable<Todo>{
    return this._http.get<Todo>(this.baseUrl+id);

  }
}
