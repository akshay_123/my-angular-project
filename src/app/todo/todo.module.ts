import { TodoRoutingModule } from './todo.routing';
import { EditComponent } from './edit/edit.component';
import { TodoComponent } from './todo.component';
import { NgToastModule } from 'ng-angular-popup';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';

@NgModule({
    declarations:[
        TodoComponent,
        EditComponent
    ],
    imports:[
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        NgToastModule,
        TodoRoutingModule
        
    ],
    providers: [
       
      ],
    exports: [
        TodoComponent,
        EditComponent

      ]
})
export class TodoModule{}