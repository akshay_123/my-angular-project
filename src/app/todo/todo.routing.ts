import { TodoeditguardService } from './todoeditguard.service';
import { TodoresolverserviceService } from './todoresolverservice.service';
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { EditComponent } from "./edit/edit.component";
import { TodoComponent } from "./todo.component";

const appRoutes: Routes = [
  {
    path:'',
  children:[
      {
        path:'',
        component:TodoComponent,
        resolve:{todos:TodoresolverserviceService}
      },
    {
      path:'edit/:id',
      canDeactivate:[TodoeditguardService],
      component:EditComponent
    }
  ]},
 
];

@NgModule({
    imports: [RouterModule.forChild(appRoutes)],
    exports: [RouterModule]
  })
  export class TodoRoutingModule{}