import { NgToastService } from 'ng-angular-popup';
import { TodotaskService } from './../todotask.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Status } from '../status';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  editForm:any;
  statuses:Status[]=[
    new Status('Pending'),
    new Status('Done')
  ];
  selectedId:any;
 // {value: 'Nancy', disabled: true}
  constructor(private activatedRoute:ActivatedRoute,private todoTaskService:TodotaskService,private toast:NgToastService,private router:Router) { }

  ngOnInit(): void {
    this.initializeEditFrom();
    
}
  initializeEditFrom(){
    this.editForm=new FormGroup({
      id:new FormControl(),
      title:new FormControl(),
      status:new FormControl()
    });

    //reading value of id form url
     this.activatedRoute.paramMap.subscribe((param:ParamMap)=>{
      this.selectedId=(param.get('id'));

    });

    //call the getTaskById API and bind result to reactive form
    this.todoTaskService.getTaskById(this.selectedId).subscribe((data:any) => {
      this.editForm.patchValue({
      id:data[0].id,
      title:data[0].title,
      status:data[0].status
    });

    });


  }

  //Call to update API
  onEditSubmit(){
    // console.log(this.editForm);
    // console.log(this.editForm.value);
    this.todoTaskService.updateTask(this.editForm.value).subscribe((data:any) =>{
      this.toast.success({detail:'success message',summary:'Task Updated Succesfully',duration:5000})
      this.router.navigate(['/todo']);

    })

  }

}
