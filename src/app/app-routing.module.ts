import { AngularmaterialComponent } from './angularmaterial/angularmaterial.component';
import { SafeguardService } from './safeguard.service';
import { SignupComponent } from './signup/signup.component';
import { EmployeetemplateformComponent } from './employeetemplateform/employeetemplateform.component';
import { DepartmentContactComponent } from './department-contact/department-contact.component';
import { DepartmentOverviewComponent } from './department-overview/department-overview.component';
import { DepartmrntDetailsComponent } from './departmrnt-details/departmrnt-details.component';
import { DepartmrntListComponent } from './departmrnt-list/departmrnt-list.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { DemoComponent } from './demo/demo.component';
import { CpcParentComponent } from './cpc-parent/cpc-parent.component';
import { ViewEncapsParentsComponent } from './view-encaps-parents/view-encaps-parents.component';
import { PipeDemoComponent } from './pipe-demo/pipe-demo.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path:'', redirectTo:'/demo',pathMatch:'full'},
  {path:'demo', component: DemoComponent},
  //canActivate Router safeguard
  {path:'emptemplatefrom',canActivate:[SafeguardService],component:EmployeetemplateformComponent},
  {path:'pipe', component: PipeDemoComponent},
  {path:'view-encaps', component: ViewEncapsParentsComponent},
  {path:'cpc', component: CpcParentComponent},
 // {path:'tuso'},
  //lazily loading todo module
  //Can load router safeguard
  {path:'todo',canLoad:[SafeguardService], loadChildren:()=>import('./todo/todo.module').then(x=>x.TodoModule)},
  {path:'departments', component: DepartmrntListComponent},
  {
    path:'depdetails/:depId', 
    component: DepartmrntDetailsComponent,
    children:[
      {path:'overview' ,component:DepartmentOverviewComponent},
      {path:'contact',component:DepartmentContactComponent}
    ]
  },
  {
    path:'angular-material', 
    component:AngularmaterialComponent
  },
 // {path:'signup', component: SignupComponent},
 //Lazily loading Signup module
 {path:'signup',loadChildren:()=>import('./signup/signup.module').then(x=>x.SignupModule)},
  {path:'**', component: PagenotfoundComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
