import { CommonService } from './common.service';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
export class SpinnerinterceptorService implements HttpInterceptor {

  constructor(private commomService:CommonService,private spinner:NgxSpinnerService) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
   // console.log('inside spinner interceptor')
    //this.spinner.show();
    return next.handle(req);
  }
}
