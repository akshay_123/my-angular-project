import { DatePipe, formatDate } from "@angular/common";
import { AbstractControl, FormGroup, Validators,FormControl,FormArray } from "@angular/forms";

//export class SignUPCustomValidator{
    
   // constructor(){}
    export function usernameTypeValidator(control:AbstractControl):{[key:string]:boolean;}{
        let invalidUsernameArray=['hello','user','angular'];
        let val:string=control.value;
        let lowercaseValue:string='';
        if (!(val==null)) {
            lowercaseValue =val.toLowerCase();

        }
      //  console.log(control.value);
        if (invalidUsernameArray.indexOf(lowercaseValue)>=0) {
            return {usernameNotAllowdwd:true};
        }
        return {};
    }

    //cross field validation
    export function passwordConfirmPasswordMatcher(control:AbstractControl):{[key:string]:boolean}{
        let password=control.get('password')?.value;
       // console.log(password);
        let confirmPassword=control.get('confirmPassword')?.value;
       // console.log(confirmPassword);
        if(!(confirmPassword==password)){
        //    console.log(confirmPassword==password);
        return {confirmPasswordValidator:true};
        }
        return {};

    }

    ///code to apply runtime validation on phone and email control based on user action
    export function detectRadioButtonChangeAndApplyValidationContols(signUpForm:FormGroup,selectedValue:any){
        //console.log(signUpForm.get('userNotification')?.value);
       // console.log(selectedValue);
        const emailControl= signUpForm.get('email');
        const phoneControl= signUpForm.get('phone');
        if(selectedValue=='phone'){
            phoneControl?.setValidators([Validators.required]);
            emailControl?.clearValidators();
            emailControl?.setValidators([Validators.email]);
        } else{
            emailControl?.setValidators([Validators.required,Validators.email]);
            phoneControl?.clearValidators();
        }
        emailControl?.updateValueAndValidity();
        phoneControl?.updateValueAndValidity();
        return selectedValue;
        

    }

    //code to add form control dynamically when user click on add hobby
    export function addUserHobby(signUpForm:FormGroup):number{
        const control=new FormControl();
        control.setValidators(Validators.required);
        control.updateValueAndValidity();
        const formArray= (signUpForm.get('userHobbies') as FormArray);
        formArray.push(control);
        return formArray.length;
    }

    //code to remove form control dynamically when user click on remove hobby
    export function removeUserHobbyControl(signUpForm:FormGroup,controlIndex:number){
        (signUpForm.get('userHobbies') as FormArray).removeAt(controlIndex);

    }

    //start date end date validator
    export function startEndDateValidator(control:AbstractControl):{[key:string]:boolean}{
        const starDate=control.get('startDate');
      //  console.log(starDate?.value)
        const endDate=control.get('endDate');
      //  console.log(endDate?.value)
        if(starDate?.value>endDate?.value){
        return {InvalidDateSelection:true};
        }
        return {};
    }

    export function futureStartAndEndDateValidator(control:AbstractControl):{[key:string]:boolean}{
       let currentDate= new Date();
       let dd=String(currentDate.getDate()).padStart(2, '0');
       let mm=String(currentDate.getMonth() + 1).padStart(2, '0');
       let yyyy=currentDate.getFullYear();
       let FormattedcurrentDate=yyyy + '-' + mm + '-' + dd;
       //console.log(FormattedcurrentDate );
       //console.log(control.value)

        if(control?.value>FormattedcurrentDate){
        return {FututerStartDateSelectionNotAllowed:true};
        }
        return {};
    }

    // export function startEndDateValidator(control:AbstractControl):{[key:string]:boolean}{
    //     const starDate=control.get('startDate');
    //   //  console.log(starDate?.value)
    //     const endDate=control.get('endDate');
    //   //  console.log(endDate?.value)
    //     if(starDate?.value>endDate?.value){
    //     return {InvalidDateSelection:true};
    //     }
    //     return {};
    // }
//}