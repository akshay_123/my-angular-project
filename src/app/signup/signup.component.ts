
import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgToastService } from 'ng-angular-popup';
import { usernameTypeValidator,passwordConfirmPasswordMatcher,addUserHobby,removeUserHobbyControl, detectRadioButtonChangeAndApplyValidationContols,startEndDateValidator,futureStartAndEndDateValidator } from './signupcustomvalidator';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signupForm:any ;
  disableAddHobby:boolean=false;
  emailMandatory=true;
  phoneMandatory=false;
  radioButtonValue='email'

  constructor(private toast:NgToastService) { }

  ngOnInit(): void {
    this.initilizeReactiveSignupForm();
  }

  initilizeReactiveSignupForm(){
    this.signupForm=new FormGroup({
      username: new FormControl(null,[Validators.required,Validators.minLength(5),usernameTypeValidator.bind(this)]),
      email:new FormControl(null,[Validators.required,Validators.email]),
      //nested password form
      passwordForm: new FormGroup(
    {
      password: new FormControl(null,[Validators.required]),
      confirmPassword:new FormControl(null,[Validators.required])
    },
    [passwordConfirmPasswordMatcher.bind(this)]
    ),
    phone:new FormControl(null),
    userNotification:new FormControl('email'),
    userHobbies: new FormArray([]),
    //Nested date form
    dateForm: new FormGroup({
      startDate:new FormControl(null,[Validators.required,futureStartAndEndDateValidator.bind(this)]),
      endDate:new FormControl(null,[Validators.required,futureStartAndEndDateValidator.bind(this)])
    },[startEndDateValidator.bind(this)])
    });
  //  this.changeMandatoryInfo();
    //Detect changes on radio button when user select 
    this.signupForm.get('userNotification').valueChanges.subscribe((x:any)=> detectRadioButtonChangeAndApplyValidationContols(this.signupForm,x));
    //console.log(this.signupForm.get('userNotification'));
    
  }
  //methods to get control name
  getUsername(){
    return this.signupForm.get('username');
  }

  getEmail(){
    return this.signupForm.get('email');
  }

  getPassword(){
    return this.signupForm.get('passwordForm').get('password');
  }

  getConfirmPassword(){
    return this.signupForm.get('passwordForm').get('confirmPassword');
  }

  getPasswordFormGroup(){
    return this.signupForm.get('passwordForm');
  }
  getPhone(){
    return this.signupForm.get('phone');
  }

  getHobbies(){
    return (<FormArray>this.signupForm.get('userHobbies')).controls;
  }

  //get date control value methods
  getdateForm(){
    return this.signupForm.get('dateForm');
  }
  getStartDate(){
    return this.signupForm.get('dateForm').get('startDate');
  }
  getEndDate(){
    return this.signupForm.get('dateForm').get('endDate');
  }

  //hobby methods
  onAddHobby(){
    //call to add user hobby control dynamically
    let hobbyFormArrayLength=addUserHobby(this.signupForm);
    if(hobbyFormArrayLength>2){
      this.disableAddHobby=true;
    }

   // (this.signupForm.get('userHobbies') as FormArray).pu
  }
  onRemoveHobby(i:number){
    removeUserHobbyControl(this.signupForm,i);
    this.disableAddHobby=false;

  }
  onEmailClick(){
    this.emailMandatory=true;
    this.phoneMandatory=false;
   
  }
  onPhoneClick(){
    this.emailMandatory=false;
    this.phoneMandatory=true;
    
  }
  onSignUp(){
    this.toast.success({detail:'Success Message',summary:'User Registered Successfully',duration:5000})
    console.log(this.signupForm);
    console.log(this.signupForm.value);
   // this.signupForm.reset();
  }
}
