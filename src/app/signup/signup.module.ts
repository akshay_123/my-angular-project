import { NgToastModule } from 'ng-angular-popup';
import { SignupRoutingModule } from './signup-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SignupComponent } from './signup.component';
import { NgModule } from "@angular/core";

@NgModule({
    declarations:[
        SignupComponent
    ],
    imports:[
        CommonModule,
        ReactiveFormsModule,
        NgToastModule,
        FormsModule,
        SignupRoutingModule
    ],
    exports:[
        SignupComponent
    ]
})
export class SignupModule{}