import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appCustom]'
})
export class CustomDirective {
  b:string='';
  @Input()c:string='red';
  @HostListener("mouseenter") mouseover(){
     this.b=this.c;

  }
  @HostListener("mouseleave") mouseleave(){
     this.b='white';
  }

  @HostBinding("style.background-color") get setcolor(){
    return this.b;
  }

  constructor() { }

}
