import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HeaderhttpinterceptorService implements HttpInterceptor {

  constructor() { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('header interceptor')
    const clonedRequest=req.clone({
      setHeaders:{
        'Content-type':'application/json',
        //for test does not require
        'Auth':'eyjks'
      }
    });
    return next.handle(clonedRequest);
  }
}
