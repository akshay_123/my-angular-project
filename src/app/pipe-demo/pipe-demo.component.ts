import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipe-demo',
  templateUrl: './pipe-demo.component.html',
  styleUrls: ['./pipe-demo.component.scss']
})
export class PipeDemoComponent implements OnInit {
  name:string='Akshay Cahugule';
  tital:string='my first title'
  date:Date=new Date();
  per:number=0.149;
  per2:number=1.49;
  curr:number=123.46;
  object: Object = {foo: 'bar', baz: 'qux', nested: {xyz: 3, numbers: [1, 2, 3, 4, 5]}};

  constructor() { }

  ngOnInit(): void {
  }

}
