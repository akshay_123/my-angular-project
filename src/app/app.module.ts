import { HeaderhttpinterceptorService } from './headerhttpinterceptor.service';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DemoComponent } from './demo/demo.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ForloopDemoComponent } from './forloop-demo/forloop-demo.component';
import { CustomdirectivedemoComponent } from './customdirectivedemo/customdirectivedemo.component';
import { CustomDirective } from './custom.directive';
import { CreditcardDirective } from './creditcard.directive';
import { PipeDemoComponent } from './pipe-demo/pipe-demo.component';
import { PracticeComponent } from './practice/practice.component';
import { ViewEncapsChildComponent } from './view-encaps-child/view-encaps-child.component';
import { ViewEncapsParentsComponent } from './view-encaps-parents/view-encaps-parents.component';
import { CpcChildComponent } from './cpc-child/cpc-child.component';
import { CpcParentComponent } from './cpc-parent/cpc-parent.component';
import { HeaderComponent } from './header/header.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { DepartmrntListComponent } from './departmrnt-list/departmrnt-list.component';
import { DepartmrntDetailsComponent } from './departmrnt-details/departmrnt-details.component';
import { DepartmentOverviewComponent } from './department-overview/department-overview.component';
import { DepartmentContactComponent } from './department-contact/department-contact.component';
import { EmployeetemplateformComponent } from './employeetemplateform/employeetemplateform.component';
//import { SignupComponent } from './signup/signup.component';
import { NgToastModule } from 'ng-angular-popup';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularmaterialComponent } from './angularmaterial/angularmaterial.component';
import { MatSliderModule} from '@angular/material/slider'
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner'
import { MatChipsModule } from '@angular/material/chips'


@NgModule({
  declarations: [
    AppComponent,
    DemoComponent,
    ForloopDemoComponent,
    CustomdirectivedemoComponent,
    CustomDirective,
    CreditcardDirective,
    PipeDemoComponent,
    PracticeComponent,
    ViewEncapsChildComponent,
    ViewEncapsParentsComponent,
    CpcChildComponent,
    CpcParentComponent,
    HeaderComponent,
    PagenotfoundComponent,
    DepartmrntListComponent,
    DepartmrntDetailsComponent,
    DepartmentOverviewComponent,
    DepartmentContactComponent,
    EmployeetemplateformComponent,
    AngularmaterialComponent,
   // SignupComponent,
  ],
  
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgToastModule,
    HttpClientModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatProgressSpinnerModule,
    MatChipsModule
  ],
  providers: [
    {
      provide:HTTP_INTERCEPTORS,
      useClass:HeaderhttpinterceptorService,
      multi:true
    },
    //spinner is not working need to check
    // {
    //   provide:HTTP_INTERCEPTORS,
    //   useClass:SpinnerinterceptorService,
    //   multi:true
    // }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
