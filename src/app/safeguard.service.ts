import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SafeguardService implements CanActivate,CanLoad {

  constructor() { }

  //component will load only if user get logged in
  canLoad(route: Route, segments: UrlSegment[]): boolean {
    if (localStorage.getItem('userName')!=null) {
      console.log(localStorage.getItem('userName'))
      return true;
    } else{
      console.log(localStorage.getItem('userName'))
      alert('login first')
      return false;
    }
    
  }
    
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (localStorage.getItem('userName')!=null) {
      return true;
    } else{
      alert('login first')
      return false;
    }
    
  }
}
