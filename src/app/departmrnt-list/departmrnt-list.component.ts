import { Router, Routes, ActivatedRoute, ParamMap } from '@angular/router';
import { Department } from './department';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-departmrnt-list',
  templateUrl: './departmrnt-list.component.html',
  styleUrls: ['./departmrnt-list.component.scss']
})
export class DepartmrntListComponent implements OnInit {
  selectedId:any;

  constructor(private router:Router,private activatedRoute:ActivatedRoute) {
    //reading optional parameter
    this.activatedRoute.paramMap.subscribe((param:ParamMap)=>{
      this.selectedId=(param.get('id'));

    });
   }

  ngOnInit(): void {
  }

  depArray:Department[]=[
    new Department('1','Java'),
    new Department('2','Angular'),
    new Department('3','Python'),
    new Department('4','DotNet')
  ];
  
  

  onDepClick(item:Department){
    console.log(this.activatedRoute);
    this.router.navigate(['/depdetails',item.depId]);
    //relative path
    //this.router.navigate([item.depId],{relativeTo:this.activatedRoute});

  }

}
